//
//  DemoViewController.m
//  MessageContentParserDemo
//
//  Created by mun on 7/15/15.
//  Copyright (c) 2015 mun. All rights reserved.
//

#import "DemoViewController.h"
#import "SpecialContentParser.h"
@interface DemoViewController () <UITextFieldDelegate>

@end

@implementation DemoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [self getParseResult:nil];
    return YES;
}

- (IBAction)getParseResult:(id)sender
{
    [self.tfInput resignFirstResponder];
    [self startLoad];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
        SpecialContentParser* parser = [[SpecialContentParser alloc] init];
        NSString* result = [parser parseContent:_tfInput.text];
        dispatch_async(dispatch_get_main_queue(), ^() {
            [self stopLoad];
            _tvOutput.text = result;
        });
    });
}

- (void)startLoad
{
    self.view.userInteractionEnabled = NO;
    [_loadingIndicator startAnimating];
    _tvOutput.text=@"";
}

- (void)stopLoad
{
    self.view.userInteractionEnabled = YES;
    [_loadingIndicator stopAnimating];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
