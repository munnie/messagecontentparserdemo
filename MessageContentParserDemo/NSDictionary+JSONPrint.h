//
//  NSDictionary+JSONPrint.h
//  MessageContentParserDemo
//
//  Created by mun on 7/15/15.
//  Copyright (c) 2015 mun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (JSONPrint)
- (NSString*)jsonStringWithPrettyPrint:(BOOL)prettyPrint;
@end
