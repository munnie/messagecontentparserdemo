//
//  NSString+DetectData.h
//  MessageContentParserDemo
//
//  Created by mun on 7/16/15.
//  Copyright (c) 2015 mun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (DetectData)

+ (NSArray*)detectedStringFromString:(NSString*)input WithPattern:(NSString*)pattern AtPatternGroup:(int)group;

+ (NSString*)titleOfURLString:(NSString*)url;

- (NSString*)fixedJsonString;
@end
