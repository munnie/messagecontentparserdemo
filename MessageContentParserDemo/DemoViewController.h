//
//  DemoViewController.h
//  MessageContentParserDemo
//
//  Created by mun on 7/15/15.
//  Copyright (c) 2015 mun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DemoViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField* tfInput;

@property (weak, nonatomic) IBOutlet UITextView* tvOutput;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView* loadingIndicator;

@end
