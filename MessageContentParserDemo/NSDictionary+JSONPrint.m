//
//  NSDictionary+JSONPrint.m
//  MessageContentParserDemo
//
//  Created by mun on 7/15/15.
//  Copyright (c) 2015 mun. All rights reserved.
//

#import "NSDictionary+JSONPrint.h"
#import "NSString+DetectData.h"
@implementation NSDictionary (JSONPrint)
- (NSString*)jsonStringWithPrettyPrint:(BOOL)prettyPrint
{
    NSError* error;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:(NSJSONWritingOptions)(prettyPrint ? NSJSONWritingPrettyPrinted : 0)
                                                         error:&error];

    if (!jsonData) {
        NSLog(@"jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"{}";
    }
    else {
        NSString* jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        jsonStr = [jsonStr fixedJsonString];
        return jsonStr;
    }
}

@end
