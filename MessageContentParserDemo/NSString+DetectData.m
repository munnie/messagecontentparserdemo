//
//  NSString+DetectData.m
//  MessageContentParserDemo
//
//  Created by mun on 7/16/15.
//  Copyright (c) 2015 mun. All rights reserved.
//

#import "NSString+DetectData.h"

@implementation NSString (DetectData)

+ (NSArray*)detectedStringFromString:(NSString*)input WithPattern:(NSString*)pattern AtPatternGroup:(int)group
{
    NSMutableArray* dataArray = [[NSMutableArray alloc] init];
    NSError* error = nil;
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
    if (!error) {
        NSArray* matches = [regex matchesInString:input options:0 range:NSMakeRange(0, input.length)];
        for (NSTextCheckingResult* match in matches) {
            NSRange itemRange = [match rangeAtIndex:group];
            NSString* item = [input substringWithRange:itemRange];
            if (![dataArray containsObject:item]) {
                //check duplicated item
                [dataArray addObject:item];
            }
        }
    }
    else {
        NSLog(@"regularExpressionWithPattern error: %@", error.description);
    }
    return dataArray;
}

+ (NSString*)titleOfURLString:(NSString*)url
{
    NSError* err;
    NSString* HTMLStr = [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:&err];
    //get HTML synchronously, may change to async, depends on the app.
    //In this case, user dont have much to do while waitting for the result
    //besides, we need whole data before making json
    //so I go with 'synchronously'
    NSString* title = @"No title";
    if (!err) {
        NSArray* detectedTitles = [self detectedStringFromString:HTMLStr WithPattern:@"<title((?s).*?)>((?s).*?)<\\/title>" AtPatternGroup:2];
        //get the text between HTML <title> tag
        if ([detectedTitles count] > 0)
            title = [detectedTitles firstObject];
    }
    else {
        NSLog(@"get HTML content error: %@", err.description);
    }
    return title;
}

- (NSString*)fixedJsonString
{
    //handle special character
    NSArray* characterToFix = @[
        @[ @"\\/", @"/" ],
        @[ @"\\\\", @"\\" ],
        @[ @"\\\"", @"\"" ],
        @[ @"\\n", @"\n" ],
        @[ @"\\r", @"\r" ],
        @[ @"\\t", @"\t" ],
        @[ @"\\b", @"\b" ],
        @[ @"\\f", @"\f" ],
    ];

    NSString* jsonStr = self;
    for (NSArray* character in characterToFix) {
        jsonStr = [jsonStr stringByReplacingOccurrencesOfString:character[0] withString:character[1]];
    }
    return jsonStr;
}

@end
