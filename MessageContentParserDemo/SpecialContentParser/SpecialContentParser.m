//
//  SpecialContentParser.m
//  MessageContentParserDemo
//
//  Created by mun on 7/15/15.
//  Copyright (c) 2015 mun. All rights reserved.
//

#import "SpecialContentParser.h"
#import "NSDictionary+JSONPrint.h"
#import "NSString+DetectData.h"

@implementation SpecialContentParser

- (NSString*)parseContent:(NSString*)input
{
    NSArray* typesToDetect = @[ kDictionaryKeyMention, kDictionaryKeyEmoticon, kDictionaryKeyLink ];
    NSMutableDictionary* outputDictionary = [[NSMutableDictionary alloc] init];
    for (NSString* type in typesToDetect) {
        NSArray* detectedData = [self dataArrayWithInput:input DataType:type];
        if ([detectedData count] > 0) {
            [outputDictionary setValue:detectedData forKey:type];
        }
    }
    NSString* outputPrettyString = [outputDictionary jsonStringWithPrettyPrint:YES];
    return outputPrettyString;
}

- (NSArray*)dataArrayWithInput:(NSString*)input DataType:(NSString*)dataType
{
    NSString* pattern = nil;
    int patternGroup = 0;
    if ([dataType isEqualToString:kDictionaryKeyMention]) {
        pattern = @"@(\\w+)";
        //start with @, repeat character word until non-word
        patternGroup = 1;
    }

    else if ([dataType isEqualToString:kDictionaryKeyEmoticon]) {
        pattern = @"\\(([a-z0-9A-Z]{1,15})\\)";
        //start with (, alphabet/numeric character repeat 1->15 times, end with )
        patternGroup = 1;
    }
    else if ([dataType isEqualToString:kDictionaryKeyLink]) {
        pattern = @"((https?:\\/\\/)([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w\\.-]*)*\\/?)";
        //typical URL format
        patternGroup = 1;
    }

    NSArray* detectedData = [NSString detectedStringFromString:input WithPattern:pattern AtPatternGroup:patternGroup];
    NSMutableArray* outputArray = [[NSMutableArray alloc] init];

    if ([dataType isEqualToString:kDictionaryKeyMention] ||
        [dataType isEqualToString:kDictionaryKeyEmoticon]) {
        [outputArray addObjectsFromArray:detectedData];
    }
    else if ([dataType isEqualToString:kDictionaryKeyLink]) {
        for (NSString* item in detectedData) {
            NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] initWithObjects:@[ item ] forKeys:@[ kDictionaryKeyLinkURL ]];
            NSString* title = [NSString titleOfURLString:item];
            [dictionary setValue:title forKey:kDictionaryKeyLinkTitle];
            [outputArray addObject:dictionary];
        }
    }

    return outputArray;
}

@end
