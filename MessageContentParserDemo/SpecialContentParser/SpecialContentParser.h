//
//  SpecialContentParser.h
//  MessageContentParserDemo
//
//  Created by mun on 7/15/15.
//  Copyright (c) 2015 mun. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString* const kDictionaryKeyMention = @"mentions";
static NSString* const kDictionaryKeyEmoticon = @"emoticons";
static NSString* const kDictionaryKeyLink = @"links";
static NSString* const kDictionaryKeyLinkURL = @"url";
static NSString* const kDictionaryKeyLinkTitle = @"title";

@interface SpecialContentParser : NSObject
- (NSString*)parseContent:(NSString*)input;
- (NSArray*)dataArrayWithInput:(NSString*)input DataType:(NSString*)dataType;
@end
