//
//  MessageContentParserDemoTests.m
//  MessageContentParserDemoTests
//
//  Created by mun on 7/15/15.
//  Copyright (c) 2015 mun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "NSString+DetectData.h"
#import "SpecialContentParser.h"
@interface MessageContentParserDemoTests : XCTestCase

@end

@implementation MessageContentParserDemoTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testGetMention
{
    NSString* input = @"@nam!@ @munnie hahaha @baby @Nguyễn talks about @한국어";
    static NSArray* expectedResult = nil;
    if (expectedResult == nil)
        expectedResult = @[ @"nam", @"munnie", @"baby", @"Nguyễn", @"한국어" ];

    SpecialContentParser* parser = [SpecialContentParser new];
    NSArray* detectedData = [parser dataArrayWithInput:input DataType:kDictionaryKeyMention];

    NSSet* set1 = [NSSet setWithArray:expectedResult];
    NSSet* set2 = [NSSet setWithArray:detectedData];
    BOOL same = ([set1 isEqualToSet:set2]);
    XCTAssertTrue(same);
}

- (void)testGetEmoticon
{
    NSString* input = @"(nam)() (munnie) hahaha (baby) (Nguyễn) talks about (한국어)";
    static NSArray* expectedResult = nil;
    if (expectedResult == nil)
        expectedResult = @[ @"nam", @"munnie", @"baby" ];

    SpecialContentParser* parser = [SpecialContentParser new];
    NSArray* detectedData = [parser dataArrayWithInput:input DataType:kDictionaryKeyEmoticon];

    NSSet* set1 = [NSSet setWithArray:expectedResult];
    NSSet* set2 = [NSSet setWithArray:detectedData];
    BOOL same = ([set1 isEqualToSet:set2]);
    XCTAssertTrue(same);
}

- (void)testGetLink
{
    NSString* input = @"@nam!@ @munnie hahaha @baby http://www.google.com @Nguyễn talks about http://facebook.com @한국어 www.miumiu.org";
    static NSArray* expectedResult = nil;
    if (expectedResult == nil)
        expectedResult = @[ @"http://www.google.com", @"http://facebook.com", @"www.miumiu.org" ];

    SpecialContentParser* parser = [SpecialContentParser new];
    NSArray* detectedData = [parser dataArrayWithInput:input DataType:kDictionaryKeyLink];

    NSMutableArray* linkArray = [[NSMutableArray alloc] init];
    for (NSDictionary* data in detectedData) {
        [linkArray addObject:[data objectForKey:kDictionaryKeyLinkURL]];
    }

    NSSet* set1 = [NSSet setWithArray:expectedResult];
    NSSet* set2 = [NSSet setWithArray:linkArray];
    BOOL same = ([set1 isEqualToSet:set2]);
    XCTAssertTrue(same);
}

- (void)testPerformanceExample
{
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
